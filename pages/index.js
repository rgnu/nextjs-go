import React from 'react'
import styled from 'styled-components'
import Layout from 'layouts/Main'
import { getPosts } from 'api/posts'

import Post from 'components/Post'

const PostList = styled.ul`
  min-height: 100vh;
`

export default class IndexPage extends React.Component {
  componentDidMount () {
    window && window.dataLayer.push({
      event: 'react.componentDidMount',
      data: {
        component: 'IndexPage'
      }
    })
  }

  componentWillUpdate () {
    window && window.dataLayer.push({
      event: 'react.componentWillUpdate',
      data: {
        component: 'IndexPage'
      }
    })
  }

  render () {
    const { posts } = this.props
    return (
      <Layout>
        <PostList>
          {posts.map(p => <Post key={p.title} post={p} />)}
        </PostList>
      </Layout>
    )
  }
}

IndexPage.getInitialProps = async ({ query }) => {
  const posts = await getPosts(query)
  return { posts }
}
