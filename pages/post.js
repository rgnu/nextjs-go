import React from 'react'
import styled from 'styled-components'
import Layout from 'layouts/Main'
import { getPostById } from 'api/posts'

const Wrapper = styled.div`
  padding: 3rem;
  max-width: 750px;
  margin: 0 auto;

  @media (max-width: 750px) {
    width: 100%;
    padding: 1rem;
  }

  h1 {
    color: #222;
    font-weight: bold;
    font-size: 1.75rem;
    line-height: 35px;
    font-family: "PT Sans", sans-serif;
    text-transform: capitalize;
    margin: 0;
  }

  p {
    line-height: 28px;
    color: #666;
    font-family: "PT Sans", sans-serif;
  }
`

const PostPage = ({ post, url }) =>
  <Layout title={post.title} url={url}>
    <Wrapper>
      <h1>
        {post.title}
      </h1>
      <p>
        {post.body}
      </p>
    </Wrapper>
  </Layout>

PostPage.getInitialProps = async ({ query }) => {
  const post = await getPostById(query.id)
  return { post }
}

export default PostPage
