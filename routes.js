const nextRoutes = require('next-routes')
const routes = module.exports = nextRoutes()

routes
  .add('index', '/')
  .add('about', '/about')
  .add('contact', '/contact')
  .add('post-by-slug', '/blog/:slug', 'post')
  .add('post-by-id', '/p/:slug+-:id([0-9]+)', 'post')
