import fetch from 'isomorphic-fetch'
const esc = encodeURIComponent

export function getPosts (params) {
  const query = Object.keys(params).map(k => esc(k) + '=' + esc(params[k])).join('&')
  const url = `https://jsonplaceholder.typicode.com/posts?${query}`
  return fetch(url.toString())
  .then(res => res.json())
}

export function getPost (slug) {
  return fetch(`https://jsonplaceholder.typicode.com/posts?title=${slug}`)
  .then(res => res.json())
}

export function getPostById (id) {
  return fetch(`https://jsonplaceholder.typicode.com/posts/${id}`)
  .then(res => res.json())
}
