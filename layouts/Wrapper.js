import styled from 'styled-components'

const Wrapper = styled.footer`
  display: flex;
  min-height: 100vh;
  flex-direction: column;

  main {
    padding: 10px;
    flex: 1;
  }
`

export default Wrapper
