import React from 'react'
import { Link } from '../../routes'
import Wrapper from './Wrapper'

const PostItem = ({ post }) => (
  <Wrapper>
    <Link route='post-by-id' params={{ slug: ['verdura', post.title.replace(/[\s\W]+/g, '-')], id: post.id }}><a>
      <h3>{post.title}</h3>
      <p>{post.body}</p>
    </a></Link>
    <Link route='index' params={{userId: post.userId}}><a>user#{post.userId}</a></Link>
  </Wrapper>
)

export default PostItem
