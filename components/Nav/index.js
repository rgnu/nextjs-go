import React from 'react'
import { Link } from '../../routes'
import styled from 'styled-components'

const Wrapper = styled.nav`
  padding: 15px;
  border-bottom: 1px solid #ddd;
  display: flex;
  background: #387EF5;

  a {
    padding: 0 15px;
    color: #FFF;
  }
`

const Nav = () => (
  <Wrapper>
    <Link href='/'><a data-event='test.click.nav' data-type='home'>Home</a></Link> |
    <Link href='/about' prefetch><a data-event='test.click.nav' data-type='about'>About</a></Link> |
    <Link href='/contact' prefetch><a data-event='test.click.nav' data-type='contact'>Contact</a></Link>
  </Wrapper>
)

export default Nav
