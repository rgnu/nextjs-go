import React from 'react'
import ReactDOM from 'react-dom'
import Footer from '.'

describe('Footer', async () => {
  it('render', async () => {
    const props = { }
    const div = document.createElement('div')

    ReactDOM.render(<Footer {...props} />, div)

    const footer = div.querySelector('footer')
    expect(footer).not.toBe(null)
  })
})
